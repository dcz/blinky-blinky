Blinky-blinky
=======

Trailer lights + indicator lights.

![Front light on N1](front.jpg)

![Back combined light on N1](back.jpg)

Prototype installed on bicycle trailer KHN1 ([video](N1-trailer.MP4)).

Functionality
----------------

Back red lights and white front position light.

Independent blinking indicator lights (left/right).

Charged by USB.

Controlled by a remote.

The currently installed battery should last about 2h in perfect conditions.

Usage
--------

### Installing

The remote should be placed somewhere on the handlebars. Use the screw to tighten. Get some rubber to pad the ring.

There's no manual for installing the entire thingamajig on the trailer. If you're that far, you can figure it out.

### Turning on

1. There is a small button next to the USB cable. Press it once to make a green light shine.
2. Press ON/OFF on the remote to make a small light turn on.
3. Press buttons on the remote, see lights on the trailer go.

If the lights on the trailer don't go:

### Pairing

1. Go to the trailer. Hold the power button until the green light blinks.
2. Press ON/OFF on the remote to make a small light turn on.
3. Press any other button on the remote. The trailer lights go.

If lights don't show:

### Charging the remote

1. Turn the remote and take it out of the mount.
2. Lift the rubber flap.
3. Plug a powered USB mini-B cable into the port under the flap. A red light should appear (or the green should turn orange).

### Charging the base station

1. Bring a USB battery ("power bank") to the trailer
2. Plug the USB cable into the battery. A red light should appear (or the green should turn orange).

### Turning off

- Hold on/off on the remote until it goes dark (or red if charging)
- Hold the small button on the base station until it blinks, keep holding, then goes dark (or red if charging)

## Maintenance/repair

Don't worry about cable length - there's at least 10cm extra of each cable inside the trailer.

Cable colors:
- brown: shared Vbat (3.6V..4.6V)
- green: position lights
- white: indicators left
- yellow: indicators right

![circuit board and most important connections present or added](board.png)

There are only 2 hubs which are not soldered, one on each side. They attach cables to a small PCB using screws.

![N1 trailer cabling and resistors](plan.png)

Side lights cables are soldered.

The trailer is aluminium and the screws are threaded directly into it. Don't overtighten the screws. Rather glue things if you must.

Make sure to seal everything from water - except where the cables get plugged. Use hot glue or silicone.

The rear lights are encased in transparent silicone. If you need to replace them, cut it out of the frame with a knife. Replace with silicone again.

Don't use hot glue to fix things to aluminium, it doesn't stay attached.

## Sourcing

The lights come from a helmet from ALDI.

All 3 pieces can be removed without damaging anything. They are held by clips: side lights on top and bottom, middle only on top. Take 2 flat screwdrivers. Press one into the gap between helmet and light on the top, as close to the light as possible. Use the other as a lever to pull the light out. Careful, all lights are connected with unpluggable cables.

Use 180°C hot air on the small black plastic inset until it warps, then stop, and wedge it out with a screw driver. The lights are brighter when the black part is gone.

If the lights don't have screws, then the colored plastic might be glued to the black one. It's possible to cut them. The plastics separate when force is applied and when the orange side is treated with 180°C hot air, but so far no one managed to do it completely without cutting two sides first.

Technical bits
-----

The rear lights are combined turn/position lights. When setting them to equal current, indicators aren't visible unless you pay attention - that's BAD in traffic, because side lights aren't always visible from behind.

The solution is to dim the red lights when orange in on, and also make orange much brighter.

Each combined light has twice as many orange LEDs, driven with 4× as much current in total (it's fine, they run only a few seconds per minute), and they compete with relatively underpowered red lights when both are on: they share a 12Ω resistor, but red is choked by an extra 47Ω one.

![combined lights](combined.png)

FIXME
-------

On N1, the indicator cables going out from the base station are swapped. Oops.

The battery is kind of weak. Lead the GND, VBat, BatTmp wires out of the case and into a battery holder. Find or design a watertight single-cell Li-Ion battery holder (18650?).

Add another white position light on the other side?
